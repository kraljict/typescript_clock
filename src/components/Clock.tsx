import React from 'react';

type ClockState = {
    time: Date
}

type AcceptedProps = {
    testProp: string,
    optionalProp?: string
};

//*Props are always passed to the component first, followed by the state.
//*<{}, ClockState>, what is happening? Inside the carrot is where we pass in props to our component, as well as where we pass in the state.
//* If we are not passing is any props or state, the default values passed into the component are empty objects.
//* That would look like this: class Clock extends React.Component<{}, {}>
//* In React, you would only use Type Alias because its functionally based. 
//* Since we aren't currently passing any props to our component, we have the default empty object in place of props, denoting that we are currently not passing in props to the clock component.
//* Then, we are passing in our ClockState. 
//* Because we passed our Type Alias of ClockState into our Clock component, we not have enforced typings: When accessing this.state and when using this.setState()

class Clock extends React.Component<AcceptedProps, ClockState> {
    constructor(props: AcceptedProps){
        super(props)
         this.state = {
             time: new Date()
         }
    }

    tick() {
        this.setState({
            time: new Date()
        })
    };

    componentWillMount() {
        this.tick();
    };

    componentDidMount() {
        setInterval(() => this.tick(), 1000);
    };

    render(){
        return(
            <div>
                <h1>{this.state.time.toLocaleTimeString()}</h1>
            </div>
        )
    }
}

export default Clock;
